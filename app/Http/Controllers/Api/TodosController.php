<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Todo;
use App\Models\Todo as TodoModel;

class TodosController extends Controller
{
    public function index()
    {
        return response()->json([
            'todos' => TodoModel::all(),
        ]);
    }

    public function create(Todo $request)
    {
        $data = $request->all();
        /* removing state and views inputs to not be manipulated */
        unset($data['state']);
        unset($data['views']);

        TodoModel::create($data);

        return response()->json('success');
    }

    public function view($id)
    {
        $todo = TodoModel::with(['project', 'user'])->findOrFail($id);
        $todo->views = $todo->views + 1;
        $todo->save();

        return response()->json($todo);
    }

    public function delete($id)
    {
        $todo = TodoModel::findOrFail($id);
        $todo->delete();

        return response()->json('success');
    }

    public function done($id)
    {
        $todo = TodoModel::findOrFail($id);
        $todo->state = 'done';
        $todo->save();

        return response()->json('success');
    }
}
