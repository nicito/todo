<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User;
use App\Models\User as UserModel;

class UsersController extends Controller
{
    public function create(User $request)
    {
        UserModel::create($request->all());

        return response()->json('success');
    }
}
