<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project;
use App\Models\Project as ProjectModel;

class ProjectsController extends Controller
{
    public function index()
    {
        return response()->json([
            'projects' => ProjectModel::with(['todos'])->get(),
        ]);
    }

    public function create(Project $request)
    {
        ProjectModel::create($request->all());

        return response()->json('success');
    }
}
