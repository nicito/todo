<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\ProjectsController;
use App\Http\Controllers\Api\TodosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user', [UsersController::class, 'create']);

Route::get('/projects', [ProjectsController::class, 'index']);
Route::post('/project', [ProjectsController::class, 'create']);

Route::get('/todos', [TodosController::class, 'index']);
Route::post('/todo', [TodosController::class, 'create']);
Route::get('/todo/{id}', [TodosController::class, 'view']);
Route::delete('/todo/{id}', [TodosController::class, 'delete']);
Route::post('/todo/done/{id}', [TodosController::class, 'done']);
